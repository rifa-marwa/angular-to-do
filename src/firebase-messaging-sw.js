importScripts("https://www.gstatic.com/firebasejs/9.14.0/firebase-app-compat.js");
importScripts("https://www.gstatic.com/firebasejs/9.14.0/firebase-messaging-compat.js");


firebase.initializeApp({
  apiKey: "AIzaSyBeyk0AZsrn6ipVqrUAL2WhzrYH2j_3pWo",
  authDomain: "angular-to-do-79614.firebaseapp.com",
  databaseURL: "https://angular-to-do-79614-default-rtdb.firebaseio.com",
  projectId: "angular-to-do-79614",
  storageBucket: "angular-to-do-79614.appspot.com",
  messagingSenderId: "692897995143",
  appId: "1:692897995143:web:0f4e0e732b25e14467db22",
  measurementId: "G-8XQNYQRJX0"
});

const messaging = firebase.messaging();