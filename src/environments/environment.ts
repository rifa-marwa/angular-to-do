// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://94.74.86.174:8080/api/',
  firebase: {
    apiKey: "AIzaSyBeyk0AZsrn6ipVqrUAL2WhzrYH2j_3pWo",
    authDomain: "angular-to-do-79614.firebaseapp.com",
    databaseURL: "https://angular-to-do-79614-default-rtdb.firebaseio.com",
    projectId: "angular-to-do-79614",
    storageBucket: "angular-to-do-79614.appspot.com",
    messagingSenderId: "692897995143",
    appId: "1:692897995143:web:0f4e0e732b25e14467db22",
    measurementId: "G-8XQNYQRJX0",
    vapidKey: "BNVNuAY-u2K3rJ8Q6juHL11tyIoyILZLg-izqcSIbuMFCVbBbQYEzRnHMKv3qwG_LGDDSEWErmqXMDyZa5xbwAQ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
