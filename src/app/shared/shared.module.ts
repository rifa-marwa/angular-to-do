import { NgModule, Pipe } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './components/alert/alert.component';
import { CardComponent } from './components/card/card.component';
import { MessageComponent } from './components/message/message.component';

import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [
    AlertComponent,
    CardComponent,
    MessageComponent
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule
  ],
  exports: [
    CardComponent, 
    CommonModule,
    MessageComponent
  ] 
})
export class SharedModule { }
