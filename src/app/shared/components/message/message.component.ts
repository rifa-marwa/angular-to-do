import { Component, Inject, OnInit } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  message: any = null;

  constructor(@Inject (MAT_SNACK_BAR_DATA) public data:any){}

  ngOnInit():void {
    this.message = this.data.notification.body
  }

}
