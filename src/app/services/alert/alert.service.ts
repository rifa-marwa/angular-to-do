import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AlertComponent } from 'src/app/shared/components/alert/alert.component';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(public dialog: MatDialog) { }

  errorNotification(message:string, status:any){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
      message: message,
      status: status
    }
    this.dialog.open(AlertComponent, dialogConfig)
  }

}
