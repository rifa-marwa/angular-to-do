import { Injectable, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { MessageComponent } from 'src/app/shared/components/message/message.component';
import { getMessaging, getToken, onMessage } from "firebase/messaging"
import { environment } from 'src/environments/environment';
import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessagingService implements OnInit {

  constructor(
    private snackBar: MatSnackBar, 
    private handler: HttpBackend) {
    this.http = new HttpClient(this.handler);
  }

  message: any = null;
  url = 'https://fcm.googleapis.com/fcm/send';
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization' : 'key=AAAAoVPwWYc:APA91bE4q4KHuY50IiHPWzh7j9ozl64BVdJ_-h0R03QQCoO_Q9X7UrdwZEqLpRjsg9DU7gX8lTJVBvVtPSWSTn8mDl3zIXbPdVkm2wG802DUpJU7GJ1hVPoF0msIGCNlIIWBPYEe_rx-'
  })
  private http: HttpClient;
  private currentToken: string = ''

  requestOptions = { headers: this.headers}

  ngOnInit():void {
  }

  requestPermission(){
    const messaging = getMessaging();
    getToken(messaging, {
      vapidKey: environment.firebase.vapidKey
    }).then(
      (currentToken) => {
        if(currentToken){
            this.currentToken = currentToken
        } 
      } 
    )
  };

  sendMessageRequest(){
    this.http.post(this.url, {
      notification: {
        title: "",
        body: "Berhasil ditambahkan"
      },
      to: `${this.currentToken}`
    }, this.requestOptions).subscribe({
      next: () => {}
    })
  }

  listen(){
    const messaging = getMessaging();
    onMessage(messaging, (payload) => {
      this.message = payload
      this.openSnackBar();
    });
  }

  openSnackBar(){
    let snackBarConfig = new MatSnackBarConfig();
    snackBarConfig.data = this.message;
    snackBarConfig.duration = 4000;
    this.snackBar.openFromComponent(MessageComponent, snackBarConfig)
  }

}
