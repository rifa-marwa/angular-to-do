import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http:HttpClient, 
    public jwtHelper: JwtHelperService) { }

  public isAuthenticated(): boolean {
    const token = localStorage.getItem('id_token');
    return !this.jwtHelper.isTokenExpired(token!); //token! -> non null assertion
  }

  public login(data: any) : Observable<any>{
    return this.http.post('login', data)
  }

  public register(data: any) : Observable<any>{
    return this.http.post('register', data);
  }
}
