import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ChecklistItem } from 'src/app/interfaces/checklist-item.interface';

@Injectable({
  providedIn: 'root'
})
export class ChecklistItemService {

  endpoint: string = 'checklist';

  constructor(private http: HttpClient) { }

  getAllChecklistItem(checklistId:number){
    return this.http.get(`${this.endpoint}/${checklistId}/item`);
  }

  getOneChecklistItem(checklistId:number, checklistItemId:number){
    return this.http.get(`${this.endpoint}/${checklistId}/item/${checklistItemId}`);
  }

  createChecklistItem(data: ChecklistItem, checklistId:number){
    return this.http.post(`${this.endpoint}/${checklistId}/item`, data);
  }

  editStatusChecklistItem(data: any, checklistItemId:number, checklistId:number){
    return this.http.put(`${this.endpoint}/${checklistId}/item/${checklistItemId}`, data);
  }

  renameChecklistItem(data: ChecklistItem, checklistId:number, checklistItemId:number){
    return this.http.put(`${this.endpoint}/${checklistId}/item/rename/${checklistItemId}`, data)
  }

  deleteChecklistItem(checklistId:number, checklistItemId: number){
    return this.http.delete(`${this.endpoint}/${checklistId}/item/${checklistItemId}`);
  }
}
