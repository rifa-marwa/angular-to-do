import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Checklist } from 'src/app/interfaces/checklist.interface';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {

  endpoint: string = 'checklist';

  constructor(private http: HttpClient) { }

  getAllChecklist() : Observable<any>{
    return this.http.get(this.endpoint);
  }

  createChecklist(data: Checklist) : Observable<any>{
    return this.http.post(this.endpoint, data);
  }

  deleteChecklist(id:number){
    return this.http.delete(`${this.endpoint}/${id}`);
  }
}
