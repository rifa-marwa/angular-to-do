import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from "@angular/common/http";
import { Observable } from 'rxjs';

export class TokenInterceptor implements HttpInterceptor {
    constructor() {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setHeaders: {
                Authorization : `Bearer ${this.getToken()}`
            }
        });
        return next.handle(req);
    }

    public getToken() : string | null {
        return localStorage.getItem('id_token');
    }
}