import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from "@angular/common/http";
import { Observable } from 'rxjs';
import { environment } from "src/environments/environment";

export class ApiInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const baseUrl = req.clone({
            url: `${environment.baseUrl}${req.url}`
        });
        return next.handle(baseUrl);
    }
}