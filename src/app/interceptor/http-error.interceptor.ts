import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { AlertService } from "../services/alert/alert.service";

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private alertService: AlertService){}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return new Observable((observer) => {
            next.handle(req).subscribe({
                error: (error) => {
                    switch(error.status){
                        case 400:
                            this.alertService.errorNotification('Bad Request', error.status)
                            break;
                        case 401:
                            this.alertService.errorNotification('User tidak terautentikasi', error.status)
                            break;
                        case 404:
                            this.alertService.errorNotification('Data tidak ditemukan', error.status)
                            break;
                        case 408:
                            this.alertService.errorNotification('Request timeout', error.status)
                            break;
                        case 500:
                            this.alertService.errorNotification('Internal Server Error', error.status)
                            break;
                    }
                },
                next : (result) => {
                    if (result instanceof HttpResponse){
                        observer.next(result)
                    }
                }
            })
        })
    }

}