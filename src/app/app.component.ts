import { Component, OnInit } from '@angular/core';
import { MessagingService } from './services/messaging/messaging.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'angular-to-do';
  message: any = null;

  constructor(private messageService: MessagingService){}

  ngOnInit():void {
    this.messageService.requestPermission();
    this.messageService.listen();
  }
  
}
