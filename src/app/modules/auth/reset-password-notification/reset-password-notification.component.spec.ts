import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetPasswordNotificationComponent } from './reset-password-notification.component';

describe('ResetPasswordNotificationComponent', () => {
  let component: ResetPasswordNotificationComponent;
  let fixture: ComponentFixture<ResetPasswordNotificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResetPasswordNotificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResetPasswordNotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
