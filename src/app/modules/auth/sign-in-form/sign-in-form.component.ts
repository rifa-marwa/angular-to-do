import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.css']
})
export class SignInFormComponent implements OnInit {

  errorMessage : any;
  isError = false;
  hide = true;
  response : any;
  signInForm: FormGroup;

  constructor(
    private authApi:AuthService, 
    private formBuilder: FormBuilder,
    private route: Router,) {
    this.signInForm = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password:  new FormControl('', [Validators.required])
    })
   }

  ngOnInit(): void {}

  get password(){
    return this.signInForm.get('password')
  }

  get username(){
    return this.signInForm.get('username')
  }

  getErrorMessage(form: string) {
    switch(form){
      case 'username' : 
        if (this.username?.hasError('required')){
          return 'Username cannot be empty';
        } return ''
      case 'password' :
        if (this.password?.hasError('required')){
          return 'Password cannot be empty'
        } return '';
      default: 
        return ''
    }
  }

  onSubmit(): void {
    const data = this.signInForm.value;
    if (data.username && data.password) {
      this.authApi.login(this.signInForm.value).subscribe(
        {
          next: ((result:any) => {
            this.response = result;
            localStorage.setItem('id_token', this.response.data.token);
            this.route.navigate(['/dashboard']);
          })
        }  
      )
    } 
  }
}
