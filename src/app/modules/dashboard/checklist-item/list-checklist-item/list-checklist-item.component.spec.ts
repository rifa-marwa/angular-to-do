import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListChecklistItemComponent } from './list-checklist-item.component';

describe('ListChecklistItemComponent', () => {
  let component: ListChecklistItemComponent;
  let fixture: ComponentFixture<ListChecklistItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListChecklistItemComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ListChecklistItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
