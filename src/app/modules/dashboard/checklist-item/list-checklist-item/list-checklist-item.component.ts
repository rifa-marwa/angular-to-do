import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChecklistItemService } from 'src/app/services/checklist-item/checklist-item.service';
import { CreateChecklistItemComponent } from '../create-checklist-item/create-checklist-item.component';
import { UpdateChecklistItemComponent } from '../update-checklist-item/update-checklist-item.component';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';


@Component({
  selector: 'app-list-checklist-item',
  templateUrl: './list-checklist-item.component.html',
  styleUrls: ['./list-checklist-item.component.css']
})
export class ListChecklistItemComponent implements OnInit {

  checklistItemData: any;
  checklistItemToDo : any = [];
  checklistItemDone: any = [];
  
  constructor(
    private checklistItemApi: ChecklistItemService,
    public dialog: MatDialog,
    public dialogRef : MatDialogRef<ListChecklistItemComponent>,
    @Inject (MAT_DIALOG_DATA) public data:any
  ) { }

  ngOnInit(): void {
    this.getChecklistItem();
  }

  addItemDialog(id:number){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
        id: id
    };
    let dialog = this.dialog.open(CreateChecklistItemComponent, dialogConfig);
    dialog.afterClosed().subscribe(() => {
      this.getChecklistItem()
    })
  }


  deleteChecklistItem(checklistItemId: number){
    this.checklistItemApi.deleteChecklistItem(this.data.id, checklistItemId).subscribe({
      next: (() => {
        const todoDeleted = this.checklistItemToDo.find((item:any) => item.id == checklistItemId)
        const doneDeleted = this.checklistItemDone.find((item:any) => item.id == checklistItemId)
        this.checklistItemToDo.splice(todoDeleted, 1)
        this.checklistItemDone.splice(doneDeleted, 1)
        this.getChecklistItem();
      })
    })
  }

  drop(event: CdkDragDrop<any[]>){
    if(event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem (
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      )

      const recentDropItem = event.container.data[event.currentIndex]

      this.checklistItemApi.editStatusChecklistItem('', recentDropItem.id, this.data.id).subscribe({
        next: (() => {})
      })
    }
  }

  getChecklistItem(){
    this.checklistItemApi.getAllChecklistItem(this.data.id).subscribe({
      next: (result => {
        this.checklistItemData = result;

        this.checklistItemToDo = this.checklistItemData.data.filter((item:any) => item.itemCompletionStatus === false)
        this.checklistItemDone = this.checklistItemData.data.filter((item:any) => item.itemCompletionStatus === true)

      })
    })
  }

  openDetailDialog(id:number, name: string, status: boolean){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.data = {
        checklistItemId: id,
        checklistId: this.data.id,
        name: name,
        status: status
    };

    let dialog = this.dialog.open(UpdateChecklistItemComponent, dialogConfig);
    dialog.afterClosed().subscribe(() => {
      this.getChecklistItem()
    })
  }

  openPDF(){
    const doc = new jsPDF()
    this.getChecklistItem()
    autoTable(doc,{
      head: [['To Do']],
      body: this.checklistItemToDo.map((object:any) => {
        return [object.name]
      }),
    } )
    autoTable(doc,{
      head: [['Done']],
      body: this.checklistItemDone.map((object:any) => {
        return [object.name]
      }),
    } )
    doc.save('checklist-item.pdf')
  }

}
