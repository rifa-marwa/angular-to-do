import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ChecklistItemService } from 'src/app/services/checklist-item/checklist-item.service';

@Component({
  selector: 'app-update-checklist-item',
  templateUrl: './update-checklist-item.component.html',
  styleUrls: ['./update-checklist-item.component.css']
})
export class UpdateChecklistItemComponent implements OnInit {

  data : any;  
  errorMessage : string = '';
  itemName = new FormControl('');
  isSubmitted = false;
  submitNotification : string = '';

  constructor(
    private checklistItemApi:ChecklistItemService,
    @Inject (MAT_DIALOG_DATA) public dataImported:any
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.data = {
      itemName : this.itemName.value
    }

    this.checklistItemApi.renameChecklistItem(this.data, this.dataImported.checklistId, this.dataImported.checklistItemId).subscribe({
      next: (() => {
        this.isSubmitted = true;
        this.submitNotification = 'Berhasil diperbaharui';
      }),
      error: (error => {
        this.isSubmitted = true;
        this.errorMessage = error.message;
        this.errorMessage = this.errorMessage[0].toUpperCase() + this.errorMessage.substr(1).toLowerCase();
      })
    })
  }

}
