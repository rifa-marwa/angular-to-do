import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { ChecklistService } from 'src/app/services/checklist/checklist.service';
import { MessagingService } from 'src/app/services/messaging/messaging.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  data : any;  
  errorMessage : string = '';
  itemName = new FormControl('');
  isSubmitted = false;
  message:any;
  submitNotification : string = '';

  constructor(
    private checklistApi:ChecklistService,
    public dialogRef: MatDialogRef<CreateComponent>,
    private messageService: MessagingService
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this.data = {
      name : this.itemName.value
    }

    this.checklistApi.createChecklist(this.data).subscribe({
      next: (() => {
        this.isSubmitted = true; 
        this.messageService.sendMessageRequest()
      }),
      error: (error => {
        this.errorMessage = error.message;
        this.errorMessage = this.errorMessage[0].toUpperCase() + this.errorMessage.substr(1).toLowerCase();
      })
    })
  }

}
