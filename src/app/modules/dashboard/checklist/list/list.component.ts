import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ChecklistService } from 'src/app/services/checklist/checklist.service';
import { ListChecklistItemComponent } from '../../checklist-item/list-checklist-item/list-checklist-item.component';
import { CreateComponent } from '../create/create.component';
import { ChecklistItemService } from 'src/app/services/checklist-item/checklist-item.service';
import { BreakpointObserver } from '@angular/cdk/layout';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  checklistData:any;
  checklistDone: any[] = [];
  checklistItemData: any;
  checklistToDo: any[] = [];
  errorMessage : any;
  isMaxTabletWidth: boolean = false;

  constructor(
    private cdr: ChangeDetectorRef,
    private checklistApi:ChecklistService,
    private checklistItemApi: ChecklistItemService,
    public dialog: MatDialog,
    private observer: BreakpointObserver,
  ) { 
  }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 600px']).subscribe((result) => {
      if (result.matches) {
        this.isMaxTabletWidth = true;
      } else {
        this.isMaxTabletWidth = false;
      }
    })
    this.cdr.detectChanges();
  }

  ngOnInit(): void {
    this.getAllChecklist();
  }

  deleteChecklist(id: number){
    this.checklistApi.deleteChecklist(id).subscribe({
      next: (() => {
        const todoDeleted = this.checklistToDo.find((item:any) => item.id == id)
        const doneDeleted = this.checklistDone.find((item:any) => item.id == id)
        this.checklistToDo.splice(todoDeleted, 1)
        this.checklistDone.splice(doneDeleted, 1)
        this.getAllChecklist();
      })
    })
  }

  drop(event: CdkDragDrop<any[]>){
    if(event.previousContainer === event.container){
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem (
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      )

      const recentDropItem = event.container.data[event.currentIndex]

      this.checklistItemApi.getAllChecklistItem(recentDropItem.id).subscribe({
        next: (result => {
          this.checklistItemData = result;

          if(recentDropItem.checklistCompletionStatus === false){
            this.checklistItemData.data.forEach((item:any) => {
              if (item.itemCompletionStatus === false){
                this.checklistItemApi.editStatusChecklistItem('', item.id, recentDropItem.id).subscribe({
                  next: (() => { recentDropItem.checklistCompletionStatus = true })
                })
              }
            }) 
          } 
          else {
            this.checklistItemData.data.forEach((item:any) => {
              this.checklistItemApi.editStatusChecklistItem('', item.id, recentDropItem.id).subscribe({
                next: (() => { recentDropItem.checklistCompletionStatus = false })
              })
            })
          } 
        }) 
      })

    }
  }

  getAllChecklist(){
    this.checklistApi.getAllChecklist().subscribe({
      next: (result) => {
        this.checklistData = result;

        this.checklistToDo = this.checklistData.data.filter((item:any) => item.checklistCompletionStatus === false)
        this.checklistDone = this.checklistData.data.filter((item:any) => item.checklistCompletionStatus === true)
      }
    })
    
  }

  openAddDialog(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '20em';

    let dialog = this.dialog.open(CreateComponent, dialogConfig);
    dialog.afterClosed().subscribe({
      next: () => this.getAllChecklist()
    })
  }

  openDetailDialog(id: number, name: string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.width = '50em';
    dialogConfig.height = '90%';

    dialogConfig.data = {
        id: id,
        name: name
    };
    let dialog = this.dialog.open(ListChecklistItemComponent, dialogConfig);
    dialog.afterClosed().subscribe({
      next: () => this.getAllChecklist()
    })
  }

}
