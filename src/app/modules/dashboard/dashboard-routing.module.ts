import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './checklist/create/create.component';
import { ListComponent } from './checklist/list/list.component';

const routes: Routes = [
  {
    path: '',
    component: ListComponent
  },
  {
    path:'create-checklist',
    component: CreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
