import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { CreateComponent } from './checklist/create/create.component';
import { ListComponent } from './checklist/list/list.component';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatInputModule } from '@angular/material/input';

import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateChecklistItemComponent } from './checklist-item/create-checklist-item/create-checklist-item.component';
import { ListChecklistItemComponent } from './checklist-item/list-checklist-item/list-checklist-item.component';
import { UpdateChecklistItemComponent } from './checklist-item/update-checklist-item/update-checklist-item.component';

import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [
    CreateComponent,
    ListComponent,
    CreateChecklistItemComponent,
    ListChecklistItemComponent,
    UpdateChecklistItemComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatFormFieldModule,
    MatIconModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    DragDropModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class DashboardModule { }
