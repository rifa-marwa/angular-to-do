import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthLayoutComponent } from './layout/auth-layout/auth-layout.component';
import { DashboardLayoutComponent } from './layout/dashboard-layout/dashboard-layout.component';
import { AuthGuardService as AuthGuard } from './guard/auth-guard/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: AuthLayoutComponent,
    loadChildren:()=>import('./modules/auth/auth.module').then(mod=>mod.AuthModule)
  },
  {
    path: 'dashboard',
    component: DashboardLayoutComponent,
    canActivate: [AuthGuard],
    loadChildren:()=>import('./modules/dashboard/dashboard.module').then(mod=>mod.DashboardModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
