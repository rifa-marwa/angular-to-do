export interface Checklist{
    id : number,
    name: string,
    items: any,
    checklistCompletionStatus: boolean
}