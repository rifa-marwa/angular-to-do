export interface ChecklistItem{
    id: number,
    name: string,
    itemCompletionStatus: boolean
}