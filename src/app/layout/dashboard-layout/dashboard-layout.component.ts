import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Location } from '@angular/common';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard-layout.component.html',
  styleUrls: ['./dashboard-layout.component.css']
})
export class DashboardLayoutComponent implements OnInit {

  @ViewChild(MatSidenav) sidenav!: MatSidenav;

  constructor(
    public location: Location,
    private observer: BreakpointObserver,
    private cdr: ChangeDetectorRef
  ) {
   }

  ngAfterViewInit() {
    this.observer.observe(['(max-width: 700px']).subscribe((result) => {
      if (result.matches) {
        this.sidenav.mode = 'over';
        this.sidenav.close();
      } else {
        this.sidenav.mode = 'side';
        this.sidenav.open();
      }
    })
    this.cdr.detectChanges();
  }

  ngOnInit(): void {}

  logout(){
    localStorage.removeItem('id_token');
    localStorage.clear();
    window.location.reload();
  }

}
